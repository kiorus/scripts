BACKUP_FILE_NAME=$1  #mobile-unabank_35users.sql
RESULT_FILE_NAME=$2  #result.sql

if [ -z "$BACKUP_FILE_NAME" ] || [ -z "$RESULT_FILE_NAME" ]
then
	echo "Please use next syntax:"
	echo "./create_liquibase_script.sh <BACKUP_FILE_NAME> <RESULT_FILE_NAME>"
else
	echo "--liquibase formatted sql" > "$RESULT_FILE_NAME"

	for table_name in "public.client" \
				"public.client_phone" \
				"public.client_email" \
				"public.client_attachment" \
				"public.client_binary_content" \
				"public.client_address" \
				"public.session_client" \
				"risk.installation"
	do
		echo "" >> "$RESULT_FILE_NAME"
		cat "$BACKUP_FILE_NAME" | grep -w "INSERT INTO $table_name" >> "$RESULT_FILE_NAME"
	done
fi

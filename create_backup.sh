HOST=""
PORT="5432"
DB_NAME="unabank"
USER_NAME="orchestrator"
BACKUP_DIRECTORY_PATH="/tmp"
DATE=`date +%Y%m%d_%H%M%S`
FULL_PATH_TO_BACKUP_FILE=""

if [[ "$1" =~ ^(local|dev|qa|mob)$ ]]; then
	case $1 in
		local) HOST="localhost";;
		dev) HOST="dev-unabank.stage.itrf.tech";;
		qa) HOST="qa-unabank.stage.itrf.tech";;
		mob) HOST="mobile-unabank.stage.itrf.tech";;
	esac

	FULL_PATH_TO_BACKUP_FILE="$BACKUP_DIRECTORY_PATH/backup-$1_$DATE.sql"

	pg_dump --host="$HOST" --port="$PORT" --dbname="$DB_NAME" --username="$USER_NAME" \
		--column-inserts --format=p --file="$FULL_PATH_TO_BACKUP_FILE"
	mv "$FULL_PATH_TO_BACKUP_FILE" .
else
	echo "Please use next syntax:"
	echo "./create_backup.sh <local|dev|qa|mob>"
fi
